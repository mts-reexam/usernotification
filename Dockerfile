FROM eclipse-temurin:17.0.2_8-jdk-alpine as builder

WORKDIR /src

COPY ./src ./src
COPY ./pom.xml ./
COPY ./.mvn ./.mvn
COPY ./mvnw ./

RUN ./mvnw package

FROM eclipse-temurin:17.0.2_8-jre-alpine as runner

COPY --from=builder ./src/target/notificationUser-0.0.1-SNAPSHOT.jar ./notificationUser-0.0.1-SNAPSHOT.jar

EXPOSE 8082
ENTRYPOINT java -jar -Dspring.profiles.active=docker -Duser.timezone="Europe/Moscow" notificationUser-0.0.1-SNAPSHOT.jar