package com.notification.notificationuser.service;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ConsoleServiceTest {

    @Mock
    List<String> checkRepeatMessage;

    @Mock
    UserDTO userDTO;

    @InjectMocks
    NotificationByConsoleService service;


    @Test
    void sendMessage(){
        AbstractResponseBody response = service.sendMessage(userDTO);


        assertTrue(response instanceof SuccessResponse);
        verify(checkRepeatMessage).add(userDTO.getEmail());
    }
}
