package com.notification.notificationuser.service;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.ExceptionResponse;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private JavaMailSender mailSender;

    @InjectMocks
    private NotificationByEmailService service;

    @Mock
    private UserDTO userDTO;


    @Test
    void sendMessage_returnSuccessResponse_Test() {
        AbstractResponseBody response = service.sendMessage(userDTO);

        assertTrue(response instanceof SuccessResponse);
        verify(mailSender).send(any(SimpleMailMessage.class));
    }

    @Test
    void sendMessage_returnExceptionResponse_Test() {
        doThrow(new MailException("MailException") {
            @Override
            public String getMessage() {
                return super.getMessage();
            }
        }).when(mailSender).send(any(SimpleMailMessage.class));
        AbstractResponseBody response = service.sendMessage(userDTO);


        assertTrue(response instanceof ExceptionResponse);


        verify(mailSender).send(any(SimpleMailMessage.class));
    }
}