package com.notification.notificationuser.controller;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.service.NotificationByConsoleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)
public class ConsoleControllerTest {

    @Mock
    private NotificationByConsoleService consoleService;

    @InjectMocks
    private ConsoleController controller;


    @Mock
    UserDTO userDTO;

    @Test
    void notifyUser_Test() {
        doReturn(new SuccessResponse()).when(consoleService).sendMessage(userDTO);
        controller.notifyUser(userDTO);


        verify(consoleService).sendMessage(userDTO);
    }
}
