package com.notification.notificationuser.controller;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.lang.reflect.Field;

import static com.notification.notificationuser.util.Serializator.asJsonString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ExtendWith(MockitoExtension.class)
public class NotificationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailController emailController;

    @MockBean
    private ConsoleController consoleController;

    UserDTO userDTO = new UserDTO("user", "user@gmail.com");


    @Test
    void notifyUserByEmail_returnSuccessOrLostMessage_Test() throws Exception {
        doReturn(new ResponseEntity<AbstractResponseBody>(new SuccessResponse(), HttpStatus.OK)).when(emailController).notifyUser(any(UserDTO.class));
        mockMvc.perform(post("/notification/v0/email/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(Matchers.anyOf(
                        Matchers.is(200),
                        Matchers.is(500))))
                .andExpect(jsonPath("$.status").value(Matchers.anyOf(
                        Matchers.hasToString("OK"),
                        Matchers.hasToString("INTERNAL_SERVER_ERROR"))))
                .andExpect(jsonPath("$.trace.message").value(Matchers.anyOf(
                        Matchers.is("Успешное уведомление"),
                        Matchers.is("Сообщение утеряно " + userDTO.getEmail()))
                ));

    }

    @Test
    void notifyUserByConsole_returnSuccessOrLostMessage_Test() throws Exception {
        doReturn(new ResponseEntity<AbstractResponseBody>(new SuccessResponse(), HttpStatus.OK)).when(consoleController).notifyUser(any(UserDTO.class));
        mockMvc.perform(post("/notification/v0/console/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(Matchers.anyOf(
                        Matchers.is(200),
                        Matchers.is(500))))
                .andExpect(jsonPath("$.status").value(Matchers.anyOf(
                        Matchers.hasToString("OK"),
                        Matchers.hasToString("INTERNAL_SERVER_ERROR"))))
                .andExpect(jsonPath("$.trace.message").value(Matchers.anyOf(
                        Matchers.is("Успешное уведомление"),
                        Matchers.is("Сообщение утеряно " + userDTO.getEmail()))
                ));

    }


    @Test
    void notifyUserByEmail_nullEmail_returnException_Test() throws Exception {

        mockMvc.perform(post("/notification/v0/email/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(mockNullUser(true,false))))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").value("Возможно одно из полей содержит null"));

        verifyNoInteractions(consoleController);
    }
    @Test
    void notifyUserByIncorrectEmail_nullUsername_returnException_Test() throws Exception {

        mockMvc.perform(post("/notification/v0/email/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(mockNullUser(false,true))))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").value("Возможно одно из полей содержит null"));

        verifyNoInteractions(consoleController);
    }

    private UserDTO mockNullUser(boolean nullName, boolean nullEmail) throws IllegalAccessException {
        UserDTO userDTO = new UserDTO("","");
        for (Field field : userDTO.getClass().getDeclaredFields()){
            field.setAccessible(true);
            switch (field.getName()){
                case "email" -> field.set(userDTO,nullEmail ? null : "email@Gmail.com");
                case "username" -> field.set(userDTO,nullName ? null : "user");
            }
            field.setAccessible(false);
        }
        return userDTO;
    }

}
