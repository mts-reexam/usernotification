package com.notification.notificationuser.controller;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.service.NotificationByEmailService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class EmailControllerTest {

    @Mock
    private NotificationByEmailService emailService;

    @InjectMocks
    private EmailController controller;


    @Mock
    UserDTO userDTO;

    @Test
    void notifyUser_test() {
        doReturn(new SuccessResponse()).when(emailService).sendMessage(userDTO);
        controller.notifyUser(userDTO);


        verify(emailService).sendMessage(userDTO);
    }
}
