package com.notification.notificationuser.controller;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.exception.LostMessageException;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping(value = "/notification/v0")
public class NotificationController {

    private final EmailController emailController;


    private final ConsoleController consoleController;

    private final Random rnd = new Random();


    @Autowired
    public NotificationController(EmailController emailController, ConsoleController consoleController) {
        this.emailController = emailController;
        this.consoleController = consoleController;
    }


    @PostMapping(value = "/email/users")
    public ResponseEntity<AbstractResponseBody> notifyUserByEmail(@RequestBody UserDTO userDTO) throws LostMessageException {
        if (lottery()) {throw new LostMessageException(userDTO);}
        return emailController.notifyUser(userDTO);
    }

    @PostMapping(value = "/console/users")
    public ResponseEntity<AbstractResponseBody> notifyUserByConsole(@RequestBody UserDTO userDTO) throws LostMessageException, InterruptedException {
        Thread.sleep(rnd.nextLong(1000L,3000L));
        if (lottery()) {throw new LostMessageException(userDTO);}
        return consoleController.notifyUser(userDTO);
    }



    private boolean lottery(){
        return rnd.nextInt(0,10) < 7;
    }

}
