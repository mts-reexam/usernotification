package com.notification.notificationuser.controller;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import com.notification.notificationuser.service.NotificationByConsoleService;
import com.notification.notificationuser.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class ConsoleController{

    private final NotificationService consoleService;

    @Autowired
    public ConsoleController(NotificationByConsoleService consoleService){
        this.consoleService = consoleService;
    }

    public ResponseEntity<AbstractResponseBody> notifyUser(UserDTO userDTO) {
        AbstractResponseBody response = consoleService.sendMessage(userDTO);
        return new ResponseEntity<>(response, response.getStatus());
    }


}
