package com.notification.notificationuser.controller;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import com.notification.notificationuser.service.NotificationByEmailService;
import com.notification.notificationuser.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class EmailController {


    private final NotificationService emailService;

    @Autowired
    public EmailController(NotificationByEmailService emailService){
        this.emailService = emailService;
    }

    public ResponseEntity<AbstractResponseBody> notifyUser(UserDTO userDTO) {
        AbstractResponseBody response = emailService.sendMessage(userDTO);
        return new ResponseEntity<>(response,response.getStatus());
    }






}
