package com.notification.notificationuser.exception;

import com.notification.notificationuser.dto.UserDTO;

public class LostMessageException extends Exception{

    public LostMessageException(UserDTO userDTO){
        super("Сообщение утеряно " + userDTO.getEmail());
    }
}
