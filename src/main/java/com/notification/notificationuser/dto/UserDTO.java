package com.notification.notificationuser.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@Getter
@AllArgsConstructor
public class UserDTO {

    @NonNull
    private String username;

    @NonNull
    private String email;

}
