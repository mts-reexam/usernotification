package com.notification.notificationuser.exceptionhandler;

import com.notification.notificationuser.exception.LostMessageException;
import com.notification.notificationuser.responsebody.ExceptionResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import com.notification.notificationuser.responsebody.validation.ValidationExceptionResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ControllerExceptionHandler {


    @ExceptionHandler(LostMessageException.class)
    public ResponseEntity<AbstractResponseBody> handleException(LostMessageException exception) {
        AbstractResponseBody response = new ExceptionResponse(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(InterruptedException.class)
    public ResponseEntity<AbstractResponseBody> handleException(InterruptedException exception) {
        AbstractResponseBody response = new ExceptionResponse(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ResponseEntity<AbstractResponseBody> handleException(MethodArgumentNotValidException exception) {
        AbstractResponseBody response = new ValidationExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<AbstractResponseBody> handleException(HttpMessageNotReadableException exception) {
        AbstractResponseBody response = new ExceptionResponse(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }


}
