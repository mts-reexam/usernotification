package com.notification.notificationuser.service;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.stereotype.Service;

@Service
public interface NotificationService {

    AbstractResponseBody sendMessage(UserDTO userDTO);

}
