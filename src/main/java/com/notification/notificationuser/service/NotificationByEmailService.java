package com.notification.notificationuser.service;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.ExceptionResponse;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class NotificationByEmailService implements NotificationService{

    private final JavaMailSender mailSender;
    @Autowired
    public NotificationByEmailService(JavaMailSender mailSender){
        this.mailSender = mailSender;
    }

    @Override
    public AbstractResponseBody sendMessage(UserDTO userDTO){
        try {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setTo("my.spring.test.mail@gmail.com");
            simpleMailMessage.setText(String.format("Уважаемый %s! Ваша учётная запись %s активирована!",
                    userDTO.getUsername(),
                    userDTO.getEmail()));
            simpleMailMessage.setSubject(userDTO.getUsername());
            mailSender.send(simpleMailMessage);
        } catch (MailException exception) {
            return new ExceptionResponse(exception);
        }
        return new SuccessResponse();
    }
}
