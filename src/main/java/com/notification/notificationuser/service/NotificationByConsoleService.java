package com.notification.notificationuser.service;

import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.SuccessResponse;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NotificationByConsoleService implements NotificationService{


    private List<String> checkRepeatMessage = new ArrayList<>();

    @Override
    public AbstractResponseBody sendMessage(UserDTO userDTO) {
        System.out.printf(
                "%s был уведомлён %s\n",
                userDTO.getUsername(),
                checkRepeatMessage.contains(userDTO.getEmail()) ? "дважды" : ""
        );


        checkRepeatMessage.add(userDTO.getEmail());
        return new SuccessResponse();
    }
}
