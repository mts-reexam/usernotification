package com.notification.notificationuser.aspect;


import com.notification.notificationuser.responsebody.validation.ErrorMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.function.Supplier;


@Aspect
@Component
public class LoggingControllerExceptionHandlerAspect {

    private final Logger log = LogManager.getLogger(this.getClass());


    @Pointcut("execution(* com.notification.notificationuser.exceptionhandler.ControllerExceptionHandler.*(*)) && args(exception)")
    public void handleExceptionPointCut(Exception exception){}



    @After(value = "handleExceptionPointCut(exception)", argNames = "exception")
    public void afterHandleException(Exception exception){
        if (exception instanceof MethodArgumentNotValidException notValidException) {
            log.warn(String.format("Ошибка уведомления: %s",
                    notValidException.getBindingResult().getFieldErrors().stream()
                            .map(error -> new ErrorMessage(error.getField(),error.getRejectedValue(), error.getDefaultMessage()))
                            .collect((Supplier<ArrayList<String>>) ArrayList::new,
                                    (list,error) -> list.add(
                                            String.format("Поле: %s |Значение: %s |Сообщение: %s",
                                                    error.getFieldName(),
                                                    error.getRejectedValue(),
                                                    error.getMessage())
                                    ),
                                    (list1,list2) -> list2.addAll(list1))
            ));
        } else {
            log.warn(String.format("Ошибка уведомления: %s",
                    exception.getMessage()
            ));
        }

    }



}
