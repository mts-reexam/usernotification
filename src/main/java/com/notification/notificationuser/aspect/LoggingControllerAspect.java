package com.notification.notificationuser.aspect;


import com.notification.notificationuser.dto.UserDTO;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoggingControllerAspect {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Pointcut("execution(* com.notification.notificationuser.controller.NotificationController.*(*)) && args(userDTO)")
    public void notifyUserAtControllerPointCut(UserDTO userDTO) { }


    @Before(value = "notifyUserAtControllerPointCut(userDTO)", argNames = "userDTO")
    public void aroundNotifyUserMethod( UserDTO userDTO) {
        log.info(String.format("Почта: %s - попытка уведомления ",
                userDTO.getEmail()
        ));
    }



    @AfterReturning(value = "notifyUserAtControllerPointCut(userDTO)",returning = "response", argNames = "userDTO,response")
    public void afterNotifyUserMethod(UserDTO userDTO, ResponseEntity<AbstractResponseBody> response){
        log.info(String.format("Почта: %s - %s",
                userDTO.getEmail(),
                response.getStatusCode().value() == 200 ? "успешное уведомление" : "неудачное уведомление"
        ));
    }



}
