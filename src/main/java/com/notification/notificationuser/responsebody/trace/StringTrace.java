package com.notification.notificationuser.responsebody.trace;


import com.notification.notificationuser.responsebody.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StringTrace extends AbstractTrace {

    public String message;
}
