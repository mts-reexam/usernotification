package com.notification.notificationuser.responsebody.trace;

import com.notification.notificationuser.responsebody.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ListTrace extends AbstractTrace {

    protected List<?> messages;
}
