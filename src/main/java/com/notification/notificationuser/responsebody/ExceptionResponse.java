package com.notification.notificationuser.responsebody;

import com.notification.notificationuser.exception.LostMessageException;
import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import com.notification.notificationuser.responsebody.trace.StringTrace;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.mail.MailException;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ExceptionResponse extends AbstractResponseBody {

    public ExceptionResponse(LostMessageException exception) {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.trace = new StringTrace(exception.getMessage() );
    }

    public ExceptionResponse(MailException exception) {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.trace = new StringTrace(exception.getMessage());
    }

    public ExceptionResponse(InterruptedException exception) {
        this.status = HttpStatus.INTERNAL_SERVER_ERROR;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.trace = new StringTrace(exception.getMessage());
    }

    public ExceptionResponse(HttpMessageNotReadableException exception){

        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new StringTrace("Возможно одно из полей содержит null");
    }


}
