package com.notification.notificationuser.responsebody;

import com.notification.notificationuser.responsebody.abstraction.AbstractResponseBody;
import com.notification.notificationuser.responsebody.trace.StringTrace;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class SuccessResponse extends AbstractResponseBody {

    public SuccessResponse(){
        this.status = HttpStatus.OK;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.trace = new StringTrace( "Успешное уведомление");
    }
}
