package com.notification.notificationuser.responsebody.abstraction;


import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

@Getter
public abstract class AbstractResponseBody {

    protected Timestamp timestamp;
    protected HttpStatus status;
    protected AbstractTrace trace;

}
