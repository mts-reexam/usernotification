package com.notification.notificationuser.responsebody.validation;

import lombok.Getter;

import java.util.Arrays;

@Getter
public class ErrorMessage {

    private final String fieldName;
    private final String rejectedValue;
    private final String message;

    public ErrorMessage(String fieldName,Object rejectedValue,String message) {
        this.fieldName = fieldName;
        this.rejectedValue = rejectedValue instanceof String ? (String) rejectedValue : Arrays.toString(((char[]) rejectedValue));
        this.message = message;
    }



}
